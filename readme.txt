== Integração EnviaMais WC ==
  Plugin URI:        https://enviamais.com.br/
  Requires PHP:      7.2
  Author:            XPBOX
  Author URI:        https://xpbox.com.br/
  License:           GPL v2 or later
  License URI:       https://www.gnu.org/licenses/gpl-2.0.html

Plugin para integração do EnviaMais com o Woocomerce